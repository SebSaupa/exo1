def square(n):
    if type(n) not in [int, float]:
        raise TypeError("Veuillez saisir un nombre")
    return n**2
