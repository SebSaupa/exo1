from flask import Flask
import myfonction as mf

app = Flask(__name__)

@app.route("/square/<number>")
def squarePage(number):
    sqrt = mf.square(int(number))
    return "Le carré de " + number + " est " + str(sqrt)

#   démarrage du serveur
app.run(host="0.0.0.0", port=8080)