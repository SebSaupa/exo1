import unittest
from myfonction import square

class TestMyfonctionModule(unittest.TestCase): 
    def test_square(self):
        self.assertEqual(square(2), 4)
        self.assertEqual(square(-2), 4)
    
    def test_types(self):
            self.assertRaises(TypeError, square, True)
            self.assertRaises(TypeError, square, "a")
   
   